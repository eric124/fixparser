import FIXParser, {
    Field,
    Fields,
    Messages,
    Side,
    OrderTypes,
    HandlInst,
    TimeInForce,
    EncryptMethod
} from '../src/FIXParser'; // from 'fixparser';

const fixParser = new FIXParser();

/**
 * xxxxxxx - An ID in digits assigned to your account
 * you can find it with the instructions from this link:
 * 
 * https://help.ctrader.com/fix/getfix
 */
const SENDER = 'ctrader.xxxxxxx';
const TARGET = 'cServer';

const USERNAME = '[YOUR ID IN DIGITS]';
const PASSWORD = '[YOUR PASSWORD]';

function sendLogon() {
    const logon = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.Logon),
        new Field(Fields.BeginString, 'FIX.4.4'),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.ResetSeqNumFlag, 'Y'),
        new Field(Fields.TargetSubID, 'QUOTE'),
        new Field(Fields.Username, USERNAME),
        new Field(Fields.Password, PASSWORD),
        new Field(Fields.EncryptMethod, EncryptMethod.None),
        new Field(Fields.HeartBtInt, 10)
    );
    const messages = fixParser.parse(logon.encode());
    console.log('sending message', messages[0].description, messages[0].string);
    fixParser.send(logon);
}

// for sending market data request to cTrader's FIX API server
function sendMarketDataRequest() {
    const quote = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.MarketDataRequest),
        new Field(Fields.BeginString, 'FIX.4.4'),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.MarketDepth, 0),
        new Field(Fields.MDUpdateType, 0),
        new Field(Fields.NoRelatedSym, 2),
        new Field(Fields.Symbol, 1), // for cTrader's own symbol system, 1 for EURUSD 
        new Field(Fields.Symbol, 2), // 2 for GBPUSD 
        new Field(Fields.MDReqID, ++countReq),
        new Field(Fields.SubscriptionRequestType, 1),
        new Field(Fields.NoMDEntryTypes, 2),
        new Field(Fields.MDEntryType, 0),
        new Field(Fields.MDEntryType, 1)
    );
    var messages = fixParser.parse(quote.encode());; 
    console.log('sending message', messages[0].description, messages[0].string.replace(/\x01/g, '|'));
    fixParser.send(quote);
}

var countReq=0;

// for sending market data request to cTrader's FIX API server
function sendMarketDataRequest() {
    const quote = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.MarketDataRequest),
        new Field(Fields.BeginString, 'FIX.4.4'),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.MarketDepth, 0),
        new Field(Fields.MDUpdateType, 0),
        new Field(Fields.NoRelatedSym, 2),
        new Field(Fields.Symbol, 1), // for cTrader's own symbol system, 1 for EURUSD 
        new Field(Fields.Symbol, 2), // 2 for GBPUSD 
        new Field(Fields.MDReqID, ++countReq),
        new Field(Fields.SubscriptionRequestType, 1),
        new Field(Fields.NoMDEntryTypes, 2),
        new Field(Fields.MDEntryType, 0),
        new Field(Fields.MDEntryType, 1)
    );
    var messages = fixParser.parse(quote.encode());; 
    console.log('sending message', messages[0].description, messages[0].string.replace(/\x01/g, '|'));
    fixParser.send(quote);
}

/**
 * Set a proper server name that associated with your account
 * 
 * You can find it from cTrader's FIX API as mentioned above
 */
const SERVER = 'h[d].p.ctrader.com';

fixParser.connect({ host: SERVER, port: 9878, protocol: 'tcp', sender: SENDER, target: TARGET, fixVersion: 'FIX.4.4' });

fixParser.on('open', () => {
    console.log('Open');

    sendLogon();
    
    sendMarketDataRequest();

});
fixParser.on('message', (message) => {
    console.log('received message', message.description, message.string);

    // add a response when receiving HEART BEAT message
    // otherwise the connection will be cut by server
});
fixParser.on('close', () => {
    console.log('Disconnected');
});
